


let inputNum = Number(prompt("Provide a number"))


for (; inputNum >= 0; inputNum--) {
	if (inputNum <= 50) {
		break
	}
	else if (inputNum % 10 === 0) {
		console.log("The number is divisible by 10. Skipping number");
	}
	else if (inputNum % 5 === 0) {
		console.log(inputNum);
	}
}




// ---------------------------------------






      function remVowel(str) {
        let al = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"];
        let result = "";

        for (let i = 0; i < str.length; i++) {
          if (!al.includes(str[i])) {
            result += str[i];
          }
        }
        return result;
      }

      let str = "supercalifragilisticexpialidocious ";
      console.log(remVowel(str));
